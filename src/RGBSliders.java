import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.GridLayout;
import javax.swing.JSlider;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.Cursor;

public class RGBSliders extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RGBSliders frame = new RGBSliders();
					frame.setVisible(true);
					frame.setTitle("Calculadora RGB");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RGBSliders() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(2, 1, 0, 0));

		JPanel PanelColor = new JPanel();
		contentPane.add(PanelColor);

		JPanel panelSliders = new JPanel();
		contentPane.add(panelSliders);
		panelSliders.setLayout(new GridLayout(6, 1, 0, 0));

		JLabel RedLabel = new JLabel("Rojo");
		RedLabel.setHorizontalAlignment(SwingConstants.CENTER);
		RedLabel.setFont(new Font("Utopia", Font.BOLD | Font.ITALIC, 14));
		RedLabel.setForeground(Color.RED);
		panelSliders.add(RedLabel);

		JSlider RedSlider = new JSlider();
		RedSlider.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		panelSliders.add(RedSlider);
		RedSlider.setMaximum(255);
		RedSlider.setMinimum(0);
		

		JLabel GreenLabel = new JLabel("Verde");
		GreenLabel.setFont(new Font("Utopia", Font.BOLD | Font.ITALIC, 14));
		GreenLabel.setForeground(Color.GREEN);
		GreenLabel.setHorizontalAlignment(SwingConstants.CENTER);
		panelSliders.add(GreenLabel);

		JSlider GreenSlider = new JSlider();
		panelSliders.add(GreenSlider);
		GreenSlider.setMaximum(255);
		GreenSlider.setMinimum(0);

		JLabel BlueLabel = new JLabel("Blue");
		BlueLabel.setHorizontalAlignment(SwingConstants.CENTER);
		BlueLabel.setFont(new Font("Utopia", Font.BOLD | Font.ITALIC, 14));
		BlueLabel.setForeground(Color.BLUE);
		panelSliders.add(BlueLabel);

		JSlider BlueSlider = new JSlider();
		panelSliders.add(BlueSlider);
		BlueSlider.setMaximum(255);
		BlueSlider.setMinimum(0);

		RedSlider.addChangeListener(new ChangeListener() {

			public void stateChanged(ChangeEvent e) {
				PanelColor.setBackground(new Color (RedSlider.getValue(), GreenSlider.getValue(), BlueSlider.getValue()));

			}
		});
		

		GreenSlider.addChangeListener(new ChangeListener() {

			public void stateChanged(ChangeEvent e) {
				PanelColor.setBackground(new Color (RedSlider.getValue(), GreenSlider.getValue(), BlueSlider.getValue()));

			}
		});
		

		BlueSlider.addChangeListener(new ChangeListener() {

			public void stateChanged(ChangeEvent e) {
				PanelColor.setBackground(new Color (RedSlider.getValue(), GreenSlider.getValue(), BlueSlider.getValue()));

			}
		});

	}
}
